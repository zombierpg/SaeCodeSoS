<?php

include_once  'define.php';
include_once  'dbutil.php';

$Key = $_POST['code'];

GetFileCache($FileCacheData, $FileName, $FileSize,$FileType, $Key);

header('Cache-control: max-age=31536000');
header("Content-type:'$FileType'");
header('Expires: ' . gmdate('D, d M Y H:i:s', time()+31536000) . ' GMT');
header("Content-Disposition: attachment;filename=".$FileName);
header("Accept-Ranges: bytes");
header('Content-Length: ' . $FileSize);

echo $FileCacheData;