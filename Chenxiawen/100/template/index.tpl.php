<!DOCTYPE html>
<html lang="zh-CN">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" ref="favicon.ico">

<title>樱木小白的个人软件站</title>

<!-- Bootstrap core CSS -->
<link href="dist/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap theme -->
<link href="dist/css/bootstrap-theme.min.css" rel="stylesheet">

<!-- Custom styles for this template -->
<link href="css/blog.css" rel="stylesheet">

<!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
<!--[if lt IE 9]>
    <script src="assets/js/ie8-responsive-file-warning.js"></script><![endif]-->
<script src="assets/js/ie-emulation-modes-warning.js"></script>

<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body>

	<!-- Fixed navbar -->
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#"><img alt="猎聘网logo"
					src="assets/img/logo-small.png" width="110" height="40"> </a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="index.php?page_action=home">首页</a></li>
					<li><a href="index.php?page_action=about">关于</a></li>
					<li><a href="index.php?page_action=contact">联系方式</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">工具 <span
							class="caret"></span>
					</a>
						<ul class="dropdown-menu" role="menu">
							<li><a href="#">留言板</a></li>
							<li><a href="#">查询ip</a></li>
							<li><a href="#">phpinfo()</a></li>
							<li class="divider"></li>
							<li class="dropdown-header">空白标题头</li>
							<li><a href="#">空白链接</a></li>
							<li><a href="#">空白链接2</a></li>
						</ul></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-expanded="false">小游戏<span
							class="caret"></span>
							<ul class="dropdown-menu" role="menu">
								<li><a href="game/2048/">2048</a></li>
								<li><a href="game/StockAlmanac">证券经纪人老黄历</a></li>
								<li><a href="#">空白游戏</a></li>
							</ul>
					</a></li>
				</ul>
				<form class="navbar-form navbar-right" role="search">
					<div class="form-group">
						<input type="text" placeholder="邮箱" class="form-control">
					</div>
					<div class="form-group">
						<input type="password" placeholder="密码" class="form-control">
					</div>
					<button type="submit" class="btn btn-success">登陆</button>
				</form>
			</div>
			<!--/.nav-collapse -->
		</div>
	</nav>

	<div class="container" role="main">
		<div class="blog-header">
			<hr>
			<h1 class="blog-title">樱木小白的blog</h1>
			<p class="lead blog-description">本空间页面采用bootstrap编码样式.——————准备上线中</p>
		</div>

		<div class="row">

			<div class="col-sm-8 blog-main">
				<?php
				loadPage ( $page_action );
				?>

				<nav>
					<ul class="pager">
						<li><a href="#">上一页</a></li>
						<li><a href="#">下一页</a></li>
					</ul>
				</nav>

			</div>
			<!-- /.blog-main -->

			<div class="col-sm-3 col-sm-offset-1 blog-sidebar">
				<div class="sidebar-module sidebar-module-inset">
					<h4>侧边栏相关</h4>

					<p>
						这是侧边栏的文本 <em>斜体文本</em>普通文本.
					</p>
				</div>
				<div class="sidebar-module">
					<h4>存档</h4>
					<ol class="list-unstyled">
						<li><a href="#">March 2014</a></li>
						<li><a href="#">February 2014</a></li>
						<li><a href="#">January 2014</a></li>
						<li><a href="#">December 2013</a></li>
						<li><a href="#">November 2013</a></li>
						<li><a href="#">October 2013</a></li>
						<li><a href="#">September 2013</a></li>
						<li><a href="#">August 2013</a></li>
						<li><a href="#">July 2013</a></li>
						<li><a href="#">June 2013</a></li>
						<li><a href="#">May 2013</a></li>
						<li><a href="#">April 2013</a></li>
					</ol>
				</div>
				<div class="sidebar-module">
					<h4>其他-友情链接</h4>
					<ol class="list-unstyled">

						<li><a href="http://www.crsky.com/default.html" rel="home"> <img
								class="logo"
								src="http://pic.crsky.com/theme/images/crsky_logo.png"
								alt="非凡软件站">
								<p>非凡软件站</p>
						</a></li>
						<li><a href="http://www.jb51.net/" target="_blank">脚本之家下载</a></li>
						<li><a href="http://xiazai.zol.com.cn/">ZOL应用下载</a></li>
						<li><a href="http://sj.zol.com.cn/">ZOL手机应用</a></li>
						<li><a href="http://www.ddooo.com" target="_blank">多多软件站</a></li>
						<li><a href="#">Facebook</a></li>
					</ol>
				</div>
			</div>
			<!-- /.blog-sidebar -->

		</div>
		<!-- /.row -->

	</div>
	<!-- /.container -->

	<footer class="blog-footer">
		<p>
			Blog template built for <a href="http://getbootstrap.com">Bootstrap</a>
			by <a href="https://twitter.com/mdo">@mdo</a>.
		</p>

		<p>
			<a href="#">Back to top</a>
		</p>

		<!-- 站长之家统计 -->
        <script type="text/javascript">var cnzz_protocol = (("https:" == document.location.protocol) ? " https://" : " http://");document.write(unescape("%3Cspan id='cnzz_stat_icon_1254531986'%3E%3C/span%3E%3Cscript src='" + cnzz_protocol + "s11.cnzz.com/z_stat.php%3Fid%3D1254531986%26online%3D1%26show%3Dline' type='text/javascript'%3E%3C/script%3E"));</script>
	</footer>


	<!-- Bootstrap core JavaScript
================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="http://cdn.bootcss.com/jquery/1.11.2/jquery.min.js"></script>
	<!--国内cdn jquery 加速-->
	<script src="dist/js/bootstrap.min.js"></script>
	<script src="assets/js/docs.min.js"></script>
	<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
	<script src="assets/js/ie10-viewport-bug-workaround.js"></script>



</body>
</html>
