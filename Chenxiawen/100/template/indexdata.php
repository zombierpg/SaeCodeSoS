<div class="blog-post">
	<h2 class="blog-post-title">个人介绍</h2>
	<p class="blog-post-meta">
		2015年5月15日 by <a href="#">Super999</a>
	</p>
	<p>
		本站点空间来源于 <a href="http://sae.sina.com.cn/">新浪SAE</a>
	</p>
	<p>
		个人新浪博客地址 <a href="http://blog.sina.com.cn/super153/">樱木小白的博客</a>
	</p>
	<p>
		网址：<a href="http://blog.sina.com.cn/super153/">http://blog.sina.com.cn/super153/</a>
	</p>
	<p id="selfid"></p>
	<p>
<hr>
	</p>
	<h2>迅雷高速通道助手</h2>
	<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;迅雷高速通道助手是一款无广告，迅雷高速通道的辅助工具。当用户使用迅雷下载，使用高速通道过程中出现如：
		“该资源被举报，无法添加到高速通道”等状况。该辅助工具可以修改资源信息，使得资源可以正常下载。软件包
		小，为作者原创软件，仅用于个人学习与交流，不用于任何商业目的，希望大家多多支持，鼓励原创软件发展。</p>
	<p>
		<a href="http://blog.sina.com.cn/s/blog_557ae4980102vjcz.html">更多介绍....</a>
	</p>
	<p>
		下载地址： <a href='http://www.chenxiawen.cn/download/ThunderHelper.exe'>本站下载
		</a>&nbsp;&nbsp; &nbsp;&nbsp; <a
			href='http://www.apple9.cn/download/ThunderH/1.0/ThunderHelper.exe'>其他站点下载(apple9.cn)</a>
	</p>
</div>
<!-- /.blog-post -->
	<hr>
<div class="blog-post">
	<h2 class="blog-post-title">第一个事件</h2>

	<p class="blog-post-meta">
		2015年5月15日 by <a href="#">Super999</a>
	</p>

	<p>看了两天终于创建了这个页面</p>
	<hr>
	<p>
		这个文章中可以包含 <a href="#">连接</a>等其他内容
	</p>
	<blockquote>
		<p>
			这是默认样式的引用 <strong>这是强调样式的应用</strong> .
		</p>
	</blockquote>
	<p>
		也可以包含 <em>斜体文字</em> 和正常文字.
	</p>
	<h2>这也是一个标题</h2>
	<p>包含了正文.</p>
	<h3>和子标题</h3>
	<p>文章中嵌套了其他元素，包括 引用代码</p>
	<pre><code> 样例代码- 引用代码</code><p> int main()</p><p> { ... </p></pre>
	<p>文本支持标签，非顺序标签</p>
	<li>标签.</li>
	<li>标签.</li>
	<li>标签.</li>
	</ul>
	<p>带编号的标签.</p>
	<ol>
		<li>标签.</li>
		<li>标签..</li>
		<li>标签.</li>
	</ol>
	<p>段落就这样结束了.</p>
</div>